import argparse
import requests
import json

ch = {'t':'t','view':'view', 'help':'help', 'balance':'balance'}


def create_transaction(s, r, a):
        dictionary = {'id' : int(r),'amount' : int(a)}
        body = json.dumps(dictionary)
        res = requests.post('http://localhost:500'+str(s)+'/createtransaction', data = body )
        print(res)
        return True
        
def view_transactions(id):
        res = requests.get('http://localhost:500'+str(id)+'/chain' )
        a = res.json()
        print(a['chain'][-1])
        return True

def balance(id):
        res = requests.get('http://localhost:500'+str(id)+'/getbalance' )
        a = res.json()
        print(a)
        return True


def foo(ar):
        print("-----")
        print(ar['command'])
        if (ar['command'] == 't'):
                if (len(ar['values']) == 3):
                        sender = ar['values'][0]
                        receiver = ar['values'][1]
                        amount = ar['values'][2]
                        create_transaction(sender, receiver, amount)
                else:
                        print("Need 2 values")
        elif ar['command'] == 'view':
                if (len(ar['values']) == 0):
                        id = ar['myid'][0]
                        view_transactions(id)
                else:
                        print("No argument accepted")
        elif(ar['command'] == 'balance'):
                if (len(ar['values']) == 0):
                        id = ar['myid'][0]
                        balance(id)
                else:
                        print("No argument accepted")
        elif(ar['command'] == 'help'):
                if (len(ar['values']) == 0):
                        print(" command t: USAGE: t receiver_ip amount\n",
                                "command view: see last transactions of the latest validated block\n",
                                "command balance: see wallet's balance\n",
                        "command help: explanation of commands")
                else:
                        print("No argument accepted")
        return


transaction = argparse.ArgumentParser()
transaction.add_argument('--myid')
transaction.add_argument('command', choices=ch.keys())
transaction.add_argument('values', nargs='*')
args = vars(transaction.parse_args())

foo(args)

print (args)
